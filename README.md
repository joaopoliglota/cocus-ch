# COCUS Challenge

This project was created for the Caucus Challenge. The requirements to execute this project is a workspace Gnu/Linux with Git and Terraform packages.

<br />

## Preparing the workspace:

Clone this project to your workspace and export the AWS key and AWS secret key, they are available in the attachment email that was sent:


```bash
git clone https://gitlab.com/joaopoliglota/cocus-pt.git
```
```bash
export AWS_ACCESS_KEY_ID="YOUR-AWS-KEY"
export AWS_SECRET_ACCESS_KEY="YOUR-AWS-SECRET-KEY"
```

We have to get Terraform modules and prepare the plan to build our infrastructure. 

``` bash
terraform init
terraform plan -out "infra-cocus"
```
The output will be kind of below:
<sup>
Plan: 10 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + ec2_database_private_ip  = (known after apply)
  + ec2_webserver_private_ip = (known after apply)
  + ec2_webserver_public_ip  = (known after apply)

────────────────────────────────────────────────

Saved the plan to: infra-cocus

To perform exactly these actions, run the following command to apply:
    terraform apply "infra-cocus"

</sup>
<br />

# Building the Infrastructure
<br />

Use the function _apply_ to build the infrastructure:

``` bash
terraform apply "infra-cocus"
```

The output will be kind of below:

<sup> 
aws_vpc.awslab-vpc: Creating...
aws_vpc.awslab-vpc: Creation complete after 7s [id=vpc-055b9c2fed334e8ff]
aws_internet_gateway.awslab-internet-gw: Creating...
aws_subnet.awslab-subnet-public: Creating...

....

Apply complete! Resources: 10 added, 0 changed, 0 destroyed.

Outputs:

ec2_database_private_ip = "172.16.2.229"
ec2_webserver_private_ip = "172.16.1.35"
ec2_webserver_public_ip = "18.156.155.140"
</sup>

<br />

# Access the exposed webpage (port 80)

After the process finish, a webpage has to be available in the ec2_webserver_public_ip. For example:

http://18.156.155.140/

<br />

![Alt Exposed webpage](img.png)


# To destroy infrastructure

After all validations, the infrastructure can be destroyed using the function _destroy_ (use the option _-auto-approve_ to no need confirmed). 

``` bash
terraform destroy -auto-approve
```

The output will be kind of below:

<sub> 
Plan: 0 to add, 0 to change, 10 to destroy.

Changes to Outputs:
  - ec2_database_private_ip  = "172.16.2.229" -> null
  - ec2_webserver_private_ip = "172.16.1.35" -> null
  - ec2_webserver_public_ip  = "18.156.155.140" -> null
aws_route_table_association.public_ass_route: Destroying... [id=rtbassoc-0e1c33d6f8068245a]
aws_instance.ec2_webserver: Destroying... [id=i-0c486725c31ae6b4b]
aws_instance.ec2_database: Destroying... [id=i-047bea95c4cc1ec03]

...

aws_instance.ec2_database: Destruction complete after 44s
aws_vpc.awslab-vpc: Destroying... [id=vpc-055b9c2fed334e8ff]
aws_vpc.awslab-vpc: Destruction complete after 1s

Destroy complete! Resources: 10 destroyed.
(terraform)$

</sub>

<br />

# Conclusion

In this challenge, we did the automation of the creation of a complete infrastructure on the AWS cloud as well as the possibility to destroy all of them with a short command.