# AWS AZ
provider "aws" {
  region  = "eu-central-1"
}

# Creating a VPC
resource "aws_vpc" "awslab-vpc" {
  cidr_block = "172.16.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "awslab-vpc"
  }
}

# Creating the Public Subnet
resource "aws_subnet" "awslab-subnet-public" {
  vpc_id     = aws_vpc.awslab-vpc.id
  cidr_block = "172.16.1.0/24"

  tags = {
    Name = "awslab-subnet-public"
  }
}

# Creating the Private Subnet
resource "aws_subnet" "awslab-subnet-private" {
  vpc_id     = aws_vpc.awslab-vpc.id
  cidr_block = "172.16.2.0/24"

  tags = {
    Name = "awslab-subnet-private"
  }
}

# Creating the Internet Gateway
resource "aws_internet_gateway" "awslab-internet-gw" {
  vpc_id = aws_vpc.awslab-vpc.id

  tags = {
    Name = "awslab-internet-gw"
  }
}

# Creating the Route Table
resource "aws_route_table" "awslab-rt-internet" {
  vpc_id = aws_vpc.awslab-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.awslab-internet-gw.id
  }

  tags = {
    Name = "Public Route Table"
  }
}

# Link the Route Table with Public Subnet
resource "aws_route_table_association" "public_ass_route" {
  subnet_id      = aws_subnet.awslab-subnet-public.id
  route_table_id = aws_route_table.awslab-rt-internet.id
}

# Creating the Security Group Public
resource "aws_security_group" "sec_group_public_ports" {
  name        = "sec_group_public_ports"
  description = "Security Group Public Ports: HTTP/HTTPS/ICMP/SSH"
  vpc_id      = aws_vpc.awslab-vpc.id

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = -1
    to_port          = -1
    protocol         = "icmp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "public-ports"
  }
}

# Creating the Security Private Group
resource "aws_security_group" "sec_group_private_ports" {
  name        = "sec_group_private_ports"
  description = "Security Group Private Ports: Custom DB Port/ICMP/SSH"
  vpc_id      = aws_vpc.awslab-vpc.id

  ingress {
    from_port        = 3110
    to_port          = 3110
    protocol         = "tcp"
    cidr_blocks      = ["172.16.1.0/24"]
  }

ingress {
  from_port        = 22
  to_port          = 22
  protocol         = "tcp"
  cidr_blocks      = ["172.16.1.0/24"]
}

ingress {
  from_port        = -1
  to_port          = -1
  protocol         = "icmp"
  cidr_blocks      = ["172.16.1.0/24"]
}


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "private-ports"
  }
}

# Creating the Webserver Instance
resource "aws_instance" "ec2_webserver" {
  ami = "ami-0d1ddd83282187d18"
  instance_type = "t2.micro"
  key_name = "cocus-ch"

  subnet_id = aws_subnet.awslab-subnet-public.id
  vpc_security_group_ids = [aws_security_group.sec_group_public_ports.id]

  associate_public_ip_address = true
    

  user_data = <<-EOF
  #!/bin/bash
  sudo apt update
  sudo apt install -y docker docker-compose
  sudo docker network create -d bridge cocus-net
  echo -e "services:\n  web:\n    image: nginx:stable-alpine\n    ports:\n      - 80:80\n    networks:\n      - cocus-net\n  redis:\n    image: redis:alpine\n    networks:\n      - cocus-net\nnetworks:\n  cocus-net:\n    name: cocus-net\n    driver: bridge\n" > docker-compose.yml
  sudo docker-compose up
  EOF

  tags = {
    Name = "Webserver"
  }

}

# Creating the Database Instance
resource "aws_instance" "ec2_database" {
  ami = "ami-0d1ddd83282187d18"
  instance_type = "t2.micro"
  key_name = "cocus-ch"

  subnet_id = aws_subnet.awslab-subnet-private.id
  vpc_security_group_ids = [aws_security_group.sec_group_private_ports.id]
  
  associate_public_ip_address = false

  tags = {
    Name = "Database"
  }

}