output "ec2_webserver_public_ip" {
  value = aws_instance.ec2_webserver.public_ip
  description = "The public IP from Webserver EC2 instance"
}

output "ec2_webserver_private_ip" {
  value = aws_instance.ec2_webserver.private_ip
  description = "The public IP from Webserver EC2 instance"
}

output "ec2_database_private_ip" {
  value = aws_instance.ec2_database.private_ip
  description = "The private IP from Database EC2 instance"
}